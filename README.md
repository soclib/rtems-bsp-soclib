Instructions to build the Board Support Package (BSP) with the RTEMS kernel for the SoCLib’s platforms.
=======================================================================================================

Expected environment
--------------------

WORKDIR is the base directory where all sources are available. In this
base directories, you can find too the directories with the generated
object files after installation.

The WORKDIR tree should look initially like the following:

    ${WORKDIR}/rtems-sources/
    ${WORKDIR}/rtems-binaries/

The WORKDIR tree shall look like the followings after performing all
instructions below:

    ${WORKDIR}/rtems-sources/rtems             # RTEMS kernel sources
    ${WORKDIR}/rtems-sources/rsb               # RTEMS utility scripts
    ${WORKDIR}/rtems-sources/rtems-bsp-soclib  # RTEMS BSP sources for Soclib
    ${WORKDIR}/rtems-binaries/6                # RTEMS RISC-V crosstool

Instructions for building the CROSS-TOOLCHAIN
---------------------------------------------

First, clone the sources of the different repositories needed to build
the cross-toolchain and the RTEMS kernel.

    cd ${WORKDIR}/rtems-sources
    git clone git://git.rtems.org/rtems.git rtems
    git clone git://git.rtems.org/rtems-source-builder.git rsb
    git clone git@gitlab.com:soclib/rtems-bsp-soclib.git rtems-bsp-soclib

These instructions were tested with the 6 release of RTEMS. Thus lets
create a branch in RTEMS repositories starting at the following tags:

    cd ${WORKDIR}/rtems-sources/rtems
    git checkout -b 6-soclib 6c8c774f5bc19c3c6a3270fde9f1b68edf65d5f0
    cd ${WORKDIR}/rtems-sources/rsb
    git checkout -b 6-soclib 31dd1ab4424e59e48c60dfa587e805e908d13b02

Check that you have all needed packages in your machine before going
further in these instructions. Look into the known dependencies section
below for more details.

    cd ${WORKDIR}/rtems-sources/rsb/rtems
    ../source-builder/sb-check

Lets download the sources for building the cross-compiler for our target
architecture (i.e. RISC-V):

    ARCH=riscv
    RELEASE=6

    cd ${WORKDIR}/rtems-sources/rsb/rtems
    ../source-builder/sb-set-builder --source-only-download \
        ${RELEASE}/rtems-${ARCH}

Lets now build the cross-compiler:

    mkdir ${WORKDIR}/rtems-binaries/${RELEASE}

    ../source-builder/sb-set-builder \
        --prefix=${WORKDIR}/rtems-binaries/${RELEASE} \
        ${RELEASE}/rtems-${ARCH}

To make sure that the cross-toolchain has been correctly installed, use
the following command:

    ${WORKDIR}/rtems-binaries/${RELEASE}/bin/riscv-rtems${RELEASE}-gcc --version

For more detailed information about the build process of RTEMS, you can
look into the following website:

https://ftp.rtems.org/pub/rtems

Known Dependencies
------------------

For a Debian host, make sure you have the following packages installed:
<ul>
<li>
build-essential
</li>
<li>
g++
</li>
<li>
gdb
</li>
<li>
unzip
</li>
<li>
pax
</li>
<li>
bison
</li>
<li>
flex
</li>
<li>
texinfo
</li>
<li>
python3-dev
</li>
<li>
libpython-dev
</li>
<li>
libncurses5-dev
</li>
<li>
zlib1g-dev
</li>
</ul>

Build the RTEMS kernel and the target BSP
-----------------------------------------

Add the newly installed cross-toolchain in your PATH:

    export PATH=$PATH:$WORKDIR/rtems-binaries/${RELEASE}/bin

Enable the target BSP in RTEMS

    BSP=rv32-soclib-xcache
    cd ${WORKDIR}/rtems-sources/rtems
    patch -p1 < ${WORKDIR}/rtems-sources/rtems-bsp-soclib/0001-Add-support-for-the-rv32-soclib-xcache-BSP.patch

Bootstrap the build system of the RTEMS kernel

    cd ${WORKDIR}/rtems-sources/rtems
    ./rtems-bootstrap

Configure, compile and install the RTEMS kernel along with the target
BSP

    mkdir -p ${WORKDIR}/rtems-build/build-${BSP}
    cd ${WORKDIR}/rtems-build/build-${BSP}

    # configure
    RTEMS_PREFIX=${WORKDIR}/rtems-binaries/${RELEASE}

    ${WORKDIR}/rtems-sources/rtems/configure \
        --prefix=${RTEMS_PREFIX} \
        --enable-maintainer-mode \
        --target=${ARCH}-rtems${RELEASE} \
        --enable-rtemsbsp=${BSP} \
        --enable-smp \
        --enable-posix \
        --enable-cxx

    # build (replace <N> by and actual number of parallel jobs)
    make -j<N> |& tee build.log

    # install
    make install |& tee install.log
