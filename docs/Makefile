#
# author: Cesar Fuguet-Tortolero <c.sarfuguet (at) gmail.com>
# date: October, 2020
# description: Makefile for generating documentation files in more human-friendly formats
#
MD2DOCX = pandoc -f markdown -t docx -s -o $2 $1
MD2HTML = pandoc -f markdown -t html -s -o $2 $1
MD2MD   = pandoc -f markdown -t markdown_strict -s -o $2 $1
RM      = rm -f

.PHONY: all md docx html
md: ../README.md
docx: ../README.docx
html: ../README.html
all: md docx html

.PHONY: clean
clean:
	$(RM) ../README.docx
	$(RM) ../README.html
	$(RM) ../README.md

../%.docx: %.md.in
	$(call MD2DOCX,$<,$@)

../%.html: %.md.in
	$(call MD2HTML,$<,$@)

../%.md: %.md.in
	$(call MD2MD,$<,$@)
